/**
 * This file is part of course_toolbar Moodle block plugin.
 *
 * course_toolbar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * course_toolbar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Knowledgegate.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package course_toolbar
 * @author Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
 * @copyright 2020 Académie de Versailles
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
let headOfPage = document.querySelector('div.course-content')
let toolbar = document.querySelector('.block_course_toolbar')
let content, loadingDiv, userNotifications, toolbarNotifications, errorNotification, atHeader, sticky
var currentCategory
if (toolbar) {
    content = document.getElementById('course_toolbar_tools_content')
    loadingDiv = toolbar.querySelector('.loading-icon')
    userNotifications = document.getElementById('user-notifications')
    toolbarNotifications = document.getElementById('course_toolbar_notification')
    errorNotification = toolbarNotifications.querySelector('.alert-error')
}

// Correct bug reported in https://bugs.chromium.org/p/chromium/issues/detail?id=507397 about Chrome/Chromium, Firefox and Safari
function courseToolbarCorrectFlexboxWidth(i = 0) {
    if (!toolbar) {
        return
    }

    // Avoid infinite loop for unknown reason
    if (i == 10) {
        console.log("Resizing couldn't be done.")
        return
    }
    let favoritesPanels = toolbar.querySelectorAll('div.favorites')
    let favoritesNotFirst = favoritesPanels[favoritesPanels.length - 1]
    if (favoritesNotFirst.scrollWidth == 0) {
        setTimeout(function() {
            courseToolbarCorrectFlexboxWidth(i + 1)

        }, 200)
    } else {
        if (favoritesNotFirst.clientWidth != favoritesNotFirst.scrollWidth) {
            favoritesNotFirst.style.width = (favoritesNotFirst.scrollWidth + 10) + "px"
        }
    }
}

// Place tool bar according to parameters
if (toolbar) {
    atHeader = parseInt(document.getElementById('course_toolbar_at_header').value)
    if (atHeader) {
        headOfPage.insertBefore(toolbar, headOfPage.firstChild)
        toolbar.querySelector('div.header').style.display = 'none'
        toolbar.querySelectorAll('*').forEach(element => {
            element.classList.add("at-header");
        })
        toolbar.querySelector("[data-course-toolbar-id='ActivityModules']").addEventListener('click', courseToolbarCorrectFlexboxWidth)
    }
// Cancel sticky parameter if not wanted
    sticky = parseInt(document.getElementById('course_toolbar_sticky').value)
    if (!sticky) {
        toolbar.style.position = 'relative'
    }
// Convert default value for checkbox
    toolbar.querySelectorAll('input[type="checkbox"]').forEach(input => {
        if (input.attributes.value) {
            input.checked = true
        } else {
            input.checked = false
            input.value = 1
        }
    })
}

function courseToolbarUnavailable() {
    if (!toolbar) {
        return
    }
    loadingDiv.classList.remove('hidden')
}

function courseToolbarAvailable() {
    if (!toolbar) {
        return
    }
    loadingDiv.classList.add('hidden')
}

function courseToolbarRevealTools(event) {
    if (!toolbar) {
        return
    }

    var source

    if (event === 'undefined') {
        if (currentCategory === 'undefined') {
            return
        }
        source = currentToolbar
    } else {
        source = event.srcElement || event.target
    }

    if (!source.classList.contains("category-title")) {
        return;
    }

    // Save into storage to remember when navigating
    sessionStorage.setItem('courseToolbarCurrentCategory', source.dataset.courseToolbarId)

    // Unselect all
    toolbar.querySelectorAll(".category-title").forEach(element =>
        element.classList.remove("active")
    )
    // Select the right one
    source.classList.add("active")
    let sourceId = source.dataset.courseToolbarId
    let elements_list = toolbar.querySelector('.elements_list[data-course-toolbar-id="' + sourceId + '"]')

    // Hide all
    toolbar.querySelectorAll('.elements_list').forEach(element => element.classList.add('hidden'))
    // Reveal the right one
    elements_list.classList.remove('hidden')

    if (!atHeader) {
        content = source.parentElement.insertBefore(content, source.nextSibling)
    }
}

function courseToolbarReadParams(input) {
    if (!toolbar) {
        return
    }

    let varname = input.name.replace('course-toolbar-', '')
    if (input.type === "checkbox") {
        if (input.checked) {
            return varname + "=" + input.value
        }
        return varname + "=0"
    }
    return varname + "=" + input.value
}

function courseToolbarPrintError() {
    if (!toolbar) {
        return
    }

    errorNotification.classList.remove('hidden')
    errorNotification.classList.add('in')
    setTimeout(function () {
        errorNotification.classList.remove('in')
    }, 3000)

}

async function courseToolbarAction(event, action) {
    if (!toolbar) {
        return
    }

    event.stopPropagation()

    let source = event.srcElement || event.target
    source.parentElement.querySelectorAll('input').forEach(input => {
        action += "&" + courseToolbarReadParams(input)
    })

    courseToolbarUnavailable()
    fetch(action)
        .then(function(response) {
            if (response.ok) {
                return response.json()
            } else {
                courseToolbarPrintError()
            }
        })
        .then(function (jsonResponse) {
            if (jsonResponse.error || (jsonResponse.message && jsonResponse.status != 'OK')) {
                courseToolbarPrintError()
                courseToolbarAvailable()
                return
            }
            if (jsonResponse.action) {
                let functionName = jsonResponse.action.functionname
                let functionNameWithFirstUpperCase = functionName.charAt(0).toUpperCase() + functionName.slice(1)
                window['courseToolbar' + functionNameWithFirstUpperCase](jsonResponse.action.args.url)
            } else {
                courseToolbarAvailable()
            }
        })
        .catch(function (err) {
            courseToolbarPrintError()
            courseToolbarAvailable()
        })

    // Handle non fading notifications
    setTimeout(function () {
        toolbarNotifications.innerHTML = ""
    }, 2000)
}

function courseToolbarRedirect(url) {
    if (!toolbar) {
        return
    }

    courseToolbarUnavailable()
    document.location.href = url
}

function courseToolbarReload() {
    if (!toolbar) {
        return
    }

    courseToolbarUnavailable()
    document.location.reload()
}

function courseToolbarNotificationsAllInOne() {
    if (!toolbar) {
        return
    }

    let allModifs = userNotifications.querySelectorAll('.alert-success')
    let nbModifs = allModifs.length
    toolbarNotifications.innerHTML = toolbarNotifications.innerHTML.replace('MODULES_NB', nbModifs.toString())
    if (nbModifs > 0) {
        let myNotifs = toolbarNotifications.querySelector('div')
        myNotifs.classList.remove('hidden')
        setTimeout(function () {
            myNotifs.classList.remove('in')
        }, 3000)

    }
}

if (toolbar) {
    toolbar.querySelectorAll("[data-course-toolbar-id]").forEach(
        element => element.addEventListener('click', courseToolbarRevealTools))

    let storedCategory = sessionStorage.getItem('courseToolbarCurrentCategory')
    if (storedCategory) {
        currentCategory = toolbar.querySelector("[data-course-toolbar-id='" + storedCategory + "']")
    }
    if (!currentCategory) {
        currentCategory = toolbar.querySelector("[data-course-toolbar-id]")
    }
    currentCategory.click()

    loadingDiv.classList.add('hidden')

    errorNotification.classList.add('hidden')

    if (userNotifications) {
        // Need to append into toolbar to apply hidden effect
        userNotifications = toolbarNotifications.appendChild(userNotifications)
        userNotifications.classList.add('hidden')
        courseToolbarNotificationsAllInOne()
    }
}
