<?php
/**
 * This file is part of course_toolbar Moodle block plugin.
 *
 * course_toolbar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * course_toolbar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Knowledgegate.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package course_toolbar
 * @author Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
 * @copyright 2020 Académie de Versailles
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace block_course_toolbar\output;
defined('MOODLE_INTERNAL') || die;

use plugin_renderer_base;
use renderable;

class renderer extends plugin_renderer_base {

    public function render_course_template(course_toolbar $coursetoolbar) {
        return $this->render_from_template('block_course_toolbar/CourseToolbar', $coursetoolbar->export_for_template($this));
    }
}
