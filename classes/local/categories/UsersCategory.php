<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 04/05/2020
 * Time: 16:39
 */

namespace block_course_toolbar\local\categories;


use block_course_toolbar\local\elements\LinkElement;

class UsersCategory extends Category
{
    protected function construct_elements_by_panels()
    {
        global $CFG;
        $this->panels = [
            new Panel([
                new LinkElement($CFG->wwwroot . '/user/index.php?id=' . $this->course->id,
                    get_string('enrolledusers', 'block_course_toolbar')),
                new LinkElement($CFG->wwwroot . '/grade/report/grader/index.php?id=' . $this->course->id,
                    get_string('gradeadministration', 'block_course_toolbar'))
            ])
        ];
    }
}