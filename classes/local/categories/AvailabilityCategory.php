<?php


namespace block_course_toolbar\local\categories;
use block_course_toolbar\local\elements\ActionElement;
use block_course_toolbar\local\elements\ActionParam;

class AvailabilityCategory extends Category
{
    protected function construct_elements_by_panels()
    {
        $this->panels = [
            new Panel([
                new ActionElement(
                    $this->name, 'remove_all', $this->course->id,
                    get_string('availabilityremoveall', 'block_course_toolbar')),
                new ActionElement(
                    $this->name, 'step_by_step', $this->course->id,
                    get_string('availabilitystepbystep', 'block_course_toolbar'),
                    [
                        new ActionParam(
                            "course-toolbar-availability-stepbystep-excludesection0",
                            get_string('availabilitystepbystepexcludesection0', 'block_course_toolbar'),
                            'checkbox',
                            1
                        )
                    ])
            ])
        ];
    }
}