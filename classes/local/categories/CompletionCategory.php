<?php


namespace block_course_toolbar\local\categories;
use block_course_toolbar\local\elements\ActionElement;

class CompletionCategory extends Category
{
    protected function construct_elements_by_panels()
    {
        $this->panels = [new Panel([
            new ActionElement($this->name, 'remove_all', $this->course->id,
                get_string('completionremove', 'block_course_toolbar')),
            new ActionElement($this->name, 'reinit', $this->course->id,
                get_string('completionreset', 'block_course_toolbar'))
        ])];
    }
}