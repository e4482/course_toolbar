<?php
/**
 * This file is part of course_toolbar Moodle block plugin.
 *
 * course_toolbar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * course_toolbar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Knowledgegate.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package course_toolbar
 * @author Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
 * @copyright 2020 Académie de Versailles
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_course_toolbar\local\categories;
use renderable;
use renderer_base;
use stdClass;
use templatable;

defined('MOODLE_INTERNAL') || die;

abstract class Category implements templatable, renderable
{
    private static $categories = [];

    private $title;
    protected $name;
    protected $course;
    protected $panels;

    abstract protected function construct_elements_by_panels();

    public function __construct($course, $name, $title) {
        $this->course = $course;
        $this->title = $title;
        $this->name = $name;
        $this->construct_elements_by_panels();
        if (empty($this->panel)) {
            $this->panel = [];
        }

        if (in_array($name, self::$categories)) {
            throw new \Exception("Category $title defined multiple times");
        }
        self::$categories[] = $name;
    }

    public function export_for_template(renderer_base $output) {
        $data = new stdClass();
        $data->id = $this->name;
        $data->title = $this->title;
        $data->panels = [];
        foreach ($this->panels as $panel) {
            $data->panels[] = $panel->export_for_template($output);
        }

        return $data;
    }
}