<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 22/04/2020
 * Time: 07:22
 */

namespace block_course_toolbar\local\categories;

use renderable;
use renderer_base;
use stdClass;
use templatable;

class Panel implements templatable, renderable
{
    private $elements;
    private $cssclass;

    public function __construct($elements, $cssclass = '')
    {
        $this->elements = $elements;
        $this->cssclass = $cssclass;
    }

    public function export_for_template(renderer_base $output) {
        $data = new stdClass();
        $data->elements = [];
        $data->cssclass = $this->cssclass;
        foreach ($this->elements as $element) {
            $data->elements[] = $element->export_for_template($output);
        }

        return $data;
    }
}