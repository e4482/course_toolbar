<?php
/**
 * This file is part of course_toolbar Moodle block plugin.
 *
 * course_toolbar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * course_toolbar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Knowledgegate.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package course_toolbar
 * @author Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
 * @copyright 2020 Académie de Versailles
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_course_toolbar\local\categories;
use block_course_toolbar\local\elements\ModuleCreationElement;

defined('MOODLE_INTERNAL') || die;

abstract class ModuleCategory extends Category
{
    abstract function is_in_category($activityclass);

    public function construct_elements_by_panels() {
        global $DB;

        // check to see if user can add menus and there are modules to add
        $modnames = get_module_types_names();
        if (empty($modnames)) {
            return;
        }

        // Set the section number as the latest section
        $sections = $DB->get_records('course_sections', ['course' => $this->course->id], 'section ASC');
        $sectionnb = 0;
        if ($sections) {
            $sectionnb = array_values($sections)[count($sections)-1]->section;
        }

        // Retrieve all modules with associated metadata
        $modules = get_module_metadata($this->course, $modnames);

        // Highlighted modules in the right category
        $highlightedmodulesstring = str_replace(' ', '', get_config('block_course_toolbar', 'highlighted_modules'));
        $highlightedmodules = explode(',', $highlightedmodulesstring);
        $favorites = [];
        foreach ($highlightedmodules as $hlmodulename) {
            // Search the module matching and checking it is in the right category
            foreach ($modules as $module) {
                if ($module->name === $hlmodulename) {
                    if ($this->is_in_category($module->archetype)) {
                        $favorites[] = new ModuleCreationElement($module->name, $this->course->id, $sectionnb);
                    }
                    break;
                }
            }
        }

        // All modules in the right category
        $othermodules = [];
        foreach ($modules as $module) {
            // Skip mapmodule as it is in navigation category
            // Also skip modules that are already in favorites
            if ($module->name === 'mapmodules' || in_array($module->name, $highlightedmodules)) {
                continue;
            }

            if ($this->is_in_category($module->archetype)) {
                $othermodules[] = new ModuleCreationElement($module->name, $this->course->id, $sectionnb);
            }
        }

        $this->panels = [];
        if (count($favorites) > 0) {
            $this->panels[] = new Panel($favorites, 'favorite');
        }
        $this->panels[] = new Panel($othermodules);
    }
}
