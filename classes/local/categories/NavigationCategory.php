<?php


namespace block_course_toolbar\local\categories;
use block_course_toolbar\local\elements\ModuleCreationElement;

class NavigationCategory extends Category
{
    protected function construct_elements_by_panels()
    {
        global $DB;
        if ($DB->get_record('modules', ['name' => 'mapmodules'])) {
            $this->panels = [new Panel([
                new ModuleCreationElement("mapmodules", $this->course->id, 0, 'bigicon')
            ])];
        } else {
            $this->panels = new Panel(null);
        }
    }
}