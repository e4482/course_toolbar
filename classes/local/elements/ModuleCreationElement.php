<?php
/**
 * This file is part of course_toolbar Moodle block plugin.
 *
 * course_toolbar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * course_toolbar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Knowledgegate.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package course_toolbar
 * @author Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
 * @copyright 2020 Académie de Versailles
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_course_toolbar\local\elements;
use renderer_base;
use stdClass;

defined('MOODLE_INTERNAL') || die;

class ModuleCreationElement extends Element
{
    private $modname;
    private $courseid;
    private $section;
    private $cssclass;

    public function __construct($modulename, $courseid, $sectionnb, $cssclass = null) {
        $this->modname = $modulename;
        $this->courseid = $courseid;
        $this->section = $sectionnb;
        $this->cssclass = $cssclass;
    }

    public function export_for_template(renderer_base $output) {
        global $CFG;
        $data = new stdClass();
        $data->url = "$CFG->wwwroot/course/modedit.php?add=$this->modname&course=$this->courseid&section=$this->section";
        $data->modulename = $this->modname;
        $data->alt = get_string('modulename', $this->modname);
        $data->moduleidentifier = get_string('modulename', $this->modname);
        $data->modulecreation = true;
        if ($this->cssclass) {
            $data->cssclass = $this->cssclass;
        }
        return $data;
    }
}