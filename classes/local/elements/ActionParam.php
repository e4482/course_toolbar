<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 28/04/2020
 * Time: 14:34
 */

namespace block_course_toolbar\local\elements;


use renderable;
use renderer_base;
use stdClass;
use templatable;

class ActionParam implements templatable, renderable
{
    public $name;
    public $label;
    public $type;
    public $defaultvalue;

    public function __construct($name, $label, $type, $defaultvalue) {
        $this->name = $name;
        $this->label = $label;
        $this->type = $type;
        $this->defaultvalue = $defaultvalue;
    }

    public function export_for_template(renderer_base $output)
    {
        $data = new stdClass();
        $data->id = "course-toolbar-$this->name";
        $data->name = $this->name;
        $data->label = $this->label;
        $data->type = $this->type;
        $data->defaultvalue = $this->defaultvalue;
        return $data;
    }
}