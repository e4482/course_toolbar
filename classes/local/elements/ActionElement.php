<?php


namespace block_course_toolbar\local\elements;
use renderer_base;
use stdClass;

class ActionElement extends Element {

    private $courseid;
    private $category;
    private $action;
    private $text;
    private $params;
    //private $icon;

    public function __construct(string $category, string $action, int $courseid, string $text, array $params = null) {
        $this->courseid = $courseid;
        $this->category = $category;
        $this->action = $action;
        $this->text = $text;
        $this->params = $params;
    }

    public function export_for_template(renderer_base $output)
    {
        global $CFG;
        $data = new stdClass();
        $data->action = "$CFG->wwwroot/blocks/course_toolbar/ajax.php?category=$this->category&action=$this->action&courseid=$this->courseid";
        if (!empty($this->icon)) {
            $data->icon = $this->icon;
            $data->alt = $this->text;
        } else {
            $data->text = $this->text;
        }
        $data->params = [];
        if (!empty($this->params)) {
            foreach ($this->params as $param) {
                $data->params[] = $param->export_for_template($output);
            }
        }

        return $data;
    }

}