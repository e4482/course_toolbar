<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 04/05/2020
 * Time: 16:43
 */

namespace block_course_toolbar\local\elements;


use renderer_base;
use stdClass;

class LinkElement extends Element
{
    public $url;
    public $text;

    public function __construct($url, $text)
    {
        $this->url = $url;
        $this->text = $text;
    }

    public function export_for_template(renderer_base $output)
    {
        $data = new stdClass();
        $data->link = (object) [
            'url' => $this->url,
            'text' => $this->text
        ];
        return $data;
    }
}