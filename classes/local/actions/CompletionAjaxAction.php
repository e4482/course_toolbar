<?php


namespace block_course_toolbar\local\actions;


use block_course_toolbar\local\actions\jsactions\ReloadJsAction;
use block_course_toolbar\local\actions\responses\ResponseOk;
use core_completion\manager;

class CompletionAjaxAction extends AjaxAction
{
    const DEFAULTS = 0;
    const NO_TRACK = 1;

    public function remove_all() {
        return $this->change_completion(self::NO_TRACK);
    }

    public function reinit() {
        return $this->change_completion(self::DEFAULTS);
    }
    
    private function change_completion($type)
    {
        global $DB;

        require_login($this->courseid);
        require_capability('moodle/course:manageactivities', \context_course::instance($this->courseid));
        $course = $DB->get_record('course', ['id' => $this->courseid], '*', MUST_EXIST);

        $manager = new manager($this->courseid);

        $nbmodules = 0;
        $changeslog = [];

        // Get default of the course sorted by module type
        foreach ($DB->get_records('modules') as $module) {
            // Get all interesting modules
            $cms = $DB->get_records('course_modules', ['course' => $this->courseid, 'module' => $module->id, 'deletioninprogress' => 0]);
            if (!empty($cms)) {
                $cmids = [];
                foreach ($cms as $cm) {
                    $cmids[] = $cm->id;
                }

                // Get completion to reapply
                switch ($type) {
                    case self::DEFAULTS:
                        $completion = manager::get_default_completion($course, $module);
                        if (!plugin_supports('mod', $module->name, FEATURE_GRADE_HAS_GRADE, false)) {
                            $completion->completionusegrade = 0;
                        }
                        break;
                    case self::NO_TRACK:
                        $completion = (object) ['completion' => COMPLETION_TRACKING_NONE];
                        break;
                    default:
                        throw new Exception("Not implemented completion type $type");
                }

                $data = new \stdClass();
                $data->cmid = $cmids;
                $nbmodules += count($cmids);
                foreach ($completion as $key => $value) {
                    $data->$key = $value;
                }

                $changeslog[$module->name] = [
                    'cmids' => $cmids,
                    'completion' => $data
                ];
                $manager->apply_completion($data, true);
            }
        }
        $response = new ResponseOk("", ['nb modules concernés' => $nbmodules, 'log' => $changeslog], new ReloadJsAction());

        return $response;
    }


}