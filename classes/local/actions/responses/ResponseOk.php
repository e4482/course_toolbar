<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 09/04/2020
 * Time: 08:33
 */

namespace block_course_toolbar\local\actions\responses;


class ResponseOk extends Response
{
    public function __construct($message = '', $data = null, $action = null)
    {
        parent::__construct("OK", $message, $data, $action);
    }

}