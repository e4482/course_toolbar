<?php


namespace block_course_toolbar\local\actions\responses;


use Exception;
use stdClass;

abstract class Response
{
    public $status;
    public $message;
    public $data;
    public $action;

    public function __construct($status, $message, $data, $action = null)
    {
        $this->status = $status;
        $this->message = $message;
        $this->data = $data;
        $this->action = $action;
    }

    public function set_error($message, $data = []) {
        $this->status = "Error";
        $this->message = $message;
        $data = (array) $data;
        $this->data = (array) $this->data;
        $this->data = array_merge($this->data, $data);
        $this->data = (object) $this->data;
    }

    public function set_action_redirect($url) {
        $this->action = new RedirectJsAction($url);
    }

    public function render() {
        return json_encode($this);
    }
}