<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 09/04/2020
 * Time: 08:37
 */

namespace block_course_toolbar\local\actions\responses;


class ResponseError extends Response
{
    public function __construct()
    {
        parent::__construct("Error", "Des erreurs se sont produites", [], null); // TODO get_string
    }

    public function add_error($errorname, $errordata = true) {
        $this->data[$errorname] = $errordata;
    }

}