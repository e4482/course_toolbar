<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 09/04/2020
 * Time: 11:08
 */

namespace block_course_toolbar\local\actions\responses;


class ResponseDebug extends Response
{
    public function __construct($message, $data)
    {
        parent::__construct("Debug", $message, $data, null);
    }
}