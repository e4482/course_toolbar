<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 09/04/2020
 * Time: 08:35
 */

namespace block_course_toolbar\local\actions\responses;

use Exception;

class ResponseException extends Response
{

    public function __construct(Exception $exception, $information = null)
    {
        parent::__construct("Exception",
            $exception->getMessage(),
            ['trace' => $exception->getTrace(), 'information' => $information],
            null);
    }
}