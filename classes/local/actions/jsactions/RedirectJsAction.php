<?php


namespace block_course_toolbar\local\actions\jsactions;


class RedirectJsAction extends JsAction
{
    public function __construct($url) {
        $this->functionname = 'redirect';
        $this->args = ['url' => $url];
    }
}