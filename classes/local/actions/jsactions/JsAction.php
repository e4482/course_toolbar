<?php


namespace block_course_toolbar\local\actions\jsactions;


class JsAction
{
    public $functionname;
    public $args;

    public function __construct()
    {
        $this->functionname = 'none';
        $this->args = [];
    }

    public function render() {
        return json_encode($this);
    }
}