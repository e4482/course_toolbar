<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 10/04/2020
 * Time: 15:49
 */

namespace block_course_toolbar\local\actions\jsactions;


class ReloadJsAction extends JsAction
{
    public function __construct()
    {
        $this->functionname = 'Reload';
        $this->args = [];
    }
}