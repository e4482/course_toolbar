<?php


namespace block_course_toolbar\local\actions;

use cm_info;
use moodle_exception;

defined('MOODLE_INTERNAL') || die;

abstract class AjaxAction {

    protected $courseid;

    public function __construct($courseid)
    {
        $this->courseid = $courseid;
    }

    /**
     * Retrieve all module infos from a course
     * @param $courseid
     * @return cm_info[]
     * @throws moodle_exception
     */
    protected function get_all_modules() {
        global $USER;
        $modinfo = get_fast_modinfo($this->courseid, $USER->id);
        return $modinfo->get_cms();

    }
}