<?php

namespace block_course_toolbar\local\actions;
use block_course_toolbar\local\actions\jsactions\RedirectJsAction;
use block_course_toolbar\local\actions\jsactions\ReloadJsAction;
use block_course_toolbar\local\actions\responses\ResponseError;
use block_course_toolbar\local\actions\responses\ResponseException;
use block_course_toolbar\local\actions\responses\ResponseOk;
use Exception;

defined('MOODLE_INTERNAL') || die;
global $CFG;
require_once ("$CFG->dirroot/course/modlib.php");

class presentationAjaxAction extends AjaxAction{


    private function make_label($text, $first = false) {
        global $DB;

        // Create a label module with default parameters
        $introeditor = [
            'text' => $text,
            'format' => FORMAT_HTML,
            'itemid' => 0
        ];
        $module = (object) [
            'modulename' => 'label',
            'course' => $this->courseid,
            'section' => 0,
            'visible' => 1,
            'introeditor' => $introeditor
        ];

        try {
            $module = \create_module($module);
            // Get newly created info
            $module = $DB->get_record('course_modules', ['id' => $module->coursemodule], '*', MUST_EXIST);
            if ($module) {
                if ($first) {
                    // Place the label module as the first module of the section
                    $section = $DB->get_record('course_sections', ['section' => 0, 'course' => $this->courseid], '*', MUST_EXIST);
                    $sequence = explode(',', $section->sequence);
                    $firstmod = null;
                    foreach ($sequence as $cm) {
                        if (!empty($cm)) {
                            $firstmod = $cm;
                            break;
                        }
                    }

                    moveto_module($module, $section, $firstmod);
                }

                $responsemessage = "";
                $action = new ReloadJsAction();
                $response = new ResponseOk($responsemessage, $module, $action);
            } else {
                $response = new ResponseError();
                $response->add_error("Le module n'a pas pu être créé");
            }
        } catch (Exception $exception) {
            $response = new ResponseException($exception);
        }


        return $response;
    }

    public function make_title() {
        return $this->make_label(get_config('block_course_toolbar', 'default_title'), true);
    }

    public function make_description() {
        return $this->make_label(get_config('block_course_toolbar', 'default_description'));
    }

    public function make_launcher() {
        // Getting first module in non 0 section
        $sections = get_fast_modinfo($this->courseid)->get_sections();
        $modulename = null;
        foreach ($sections as $sectionnumber => $moduleids) {
            if ($sectionnumber !== 0) {
                if (count($moduleids) > 0) {
                    $moduleid = array_shift($moduleids);
                    $modulename = get_fast_modinfo($this->courseid)->get_cm($moduleid)->name;
                    break;
                }
            }
        }

        // No course module yet, complete with a dummy
        if (empty($modulename)) {
            $modulename = get_string('dummymodulenameforlauncher', 'block_course_toolbar');
        }

        // Replace the module name in the template from config
        $template = get_config('block_course_toolbar', 'default_launcher');
        $content = str_replace('TARGETED_MODULE', $modulename, $template);
        return $this->make_label($content);
    }
}