<?php


namespace block_course_toolbar\local\actions;


use block_course_toolbar\local\actions\jsactions\ReloadJsAction;
use block_course_toolbar\local\actions\responses\ResponseException;
use block_course_toolbar\local\actions\responses\ResponseOk;
use context_module;
use core\event\course_module_updated;

class AvailabilityAjaxAction extends AjaxAction {

    private $modulenames;

    public function __construct($courseid)
    {
        global $DB;
        parent::__construct($courseid);
        $modulesdb = $DB->get_records('modules');
        $this->modulenames = [];
        foreach ($modulesdb as $module) {
            $this->modulenames[$module->id] = $module->name;
        }
    }

    public function remove_all() {
        require_capability('moodle/course:manageactivities', \context_course::instance($this->courseid));

        $changedmodules = [];

        try {
            $modules = $this->get_all_modules();
            // Update all modules
            foreach ($modules as $module) {
                $changedmodules[] = $modules->id;
                $this->light_update_module($module->id, false);
            }

            // Update course cache
            rebuild_course_cache($this->courseid, true);
        } catch (Exception $exception) {
            return new ResponseException($exception, ['changed modules' => $changedmodules]);
        }

        return new ResponseOk('', ['changed modules' => $changedmodules], new ReloadJsAction());
    }

    public function step_by_step() {
        global $USER;

        require_capability('moodle/course:manageactivities', \context_course::instance($this->courseid));

        // Need parameter for section 0 case
        $excludesection0 = required_param('availability-stepbystep-excludesection0', PARAM_INT);

        $modinfo = get_fast_modinfo($this->courseid, $USER->id);
        // Retrieve modules in the right order
        $sections = $modinfo->get_sections();
        $lastcmid = null;
        $lastsection = null;
        $changedmodules = [];
        foreach ($sections as $sectionnunmber => $cmidinsection) {
            // No availability on section 0
            if ($sectionnunmber == 0 && $excludesection0) {
                foreach ($cmidinsection as $cmid) {
                    $this->light_update_module($cmid, false);
                    $changedmodules[] = [
                        'id' => $cmid,
                        'excluded' => true,
                        'new availability' => null
                    ];
                }
            } else {
                $firstcm = true;
                foreach ($cmidinsection as $cmid) {
                    // Skip deleted modules
                    $cm = $modinfo->get_cm($cmid);
                    if ($cm->deletioninprogress) {
                        continue;
                    }

                    // Change availibility of modules

                    if ($lastcmid) {
                        $availibility = '{"op":"&","c":[{"type":"completion","cm":' .$lastcmid . ',"e":1}],"showc":[true]}';
                    } else {
                        $availibility = null;
                    }
                    $this->light_update_module($cmid, $availibility);
                    $changedmodules[] = [
                        'id' => $cmid,
                        'name' => $cm->name,
                        'new availability' => $availibility,
                        'completion' => $cm->completion
                    ];

                    // Change availability of section
//
//                    if ($lastcmid) {
//                        $availibility = '{"op":"&","c":[{"type":"completion","cm":' .$lastcmid . ',"e":1}],"showc":[true]}';
//
//                        // Change section availability: the same as the module
//                        if ($firstcm) {
//                            $this->light_update_module($cmid, $availibility);
//                        }
//                        $cm->set_available(false, 1, $availibility);
//                    } else {
//                        $cm->set_available(true);
//                    }

                    $firstcm = false;

                    // In case there is no completion, last cmid remains the previous one
                    if ($cm->completion !== COMPLETION_TRACKING_NONE) {
                        $lastcmid = $cmid;
                    }
                }
            }
        }

        // Update course cache
        rebuild_course_cache($this->courseid, true);

        return new ResponseOk('', ['changed modules' => $changedmodules], new ReloadJsAction());
    }

    private function light_update_module($cmid, $availability) {
        global $DB;

        // Updating directly DB as no grades or views impact
        $moduleinfo = $DB->get_record('course_modules', ['id' => $cmid], '*', MUST_EXIST);
        $moduleinfo->availability = $availability;
        $moduleinfo->timemodified = time();
        $DB->update_record('course_modules', $moduleinfo);

        // Trigger update cm event to purge impacted caches
        $context = context_module::instance($cmid, MUST_EXIST);
        $moduleinfo->modname = $this->modulenames[$moduleinfo->module];
        $moduleinfo->name = $DB->get_record($moduleinfo->modname, ['id' => $moduleinfo->instance], 'name', MUST_EXIST)->name;
        course_module_updated::create_from_cm($moduleinfo, $context)->trigger();
    }
}