<?php
/**
 * This file is part of course_toolbar Moodle block plugin.
 *
 * course_toolbar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * course_toolbar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Knowledgegate.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package course_toolbar
 * @author Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
 * @copyright 2020 Académie de Versailles
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


namespace block_course_toolbar\local;
use block_course_toolbar\local\categories\ActivityModuleCategory;
use block_course_toolbar\local\categories\NavigationCategory;
use block_course_toolbar\local\categories\PresentationCategory;
use block_course_toolbar\local\categories\ResourceModuleCategory;
use block_course_toolbar\local\categories\CompletionCategory;
use block_course_toolbar\local\categories\AvailabilityCategory;
use block_course_toolbar\local\categories\UsersCategory;
use renderable;
use renderer_base;
use stdClass;
use templatable;

defined('MOODLE_INTERNAL') || die;

class CourseToolbar implements renderable, templatable {
    private $course;
    private $categories;
    private $params;

    public function __construct($course) {

        $this->course = $course;
        $this->categories = [];
        $this->params = [];

        // Parameters
        $this->params[] = (object) [
            'name' => 'at_header',
            'value' => get_config('block_course_toolbar', 'allowmovingtotop')
        ];
        $this->params[] = (object) [
            'name' => 'sticky',
            'value' => get_config('block_course_toolbar', 'stickybar')
        ];

        // Categories
        $this->categories[] = new PresentationCategory($this->course, "Presentation", get_string('presentation', 'block_course_toolbar'));
        $this->categories[] = new NavigationCategory($this->course, "Navigation", get_string('navigation', 'block_course_toolbar'));
        $this->categories[] = new ResourceModuleCategory($this->course, "ResourcesModules", get_string('resourcemodules', 'block_course_toolbar'));
        $this->categories[] = new ActivityModuleCategory($this->course, "ActivityModules", get_string('activitymodules', 'block_course_toolbar'));
        $this->categories[] = new CompletionCategory($this->course, "Completion", get_string('completion', 'block_course_toolbar'));
        $this->categories[] = new AvailabilityCategory($this->course, "Availability", get_string('availability', 'block_course_toolbar'));
        $this->categories[] = new UsersCategory($this->course, "Users", get_string('users', 'block_course_toolbar'));
    }

    public function export_for_template(renderer_base $output) {
        $data = new stdClass();
        $categoriesexport = [];
        foreach ($this->categories as $category) {
            $categoriesexport[] = $category->export_for_template($output);
        }
        $data->categories = $categoriesexport;
        $data->params = $this->params;
        $data->notification = (object) [
            'message' => "MODULES_NB " . get_string('modifiedmodulesnotification', 'block_course_toolbar'),
            'extraclasses' => 'hidden course-toolbar-notification'
        ];
        $data->error = (object) [
            'message' => get_string('ajaxerrormessage', 'block_course_toolbar'),
            'extraclasses' => 'hidden alert-danger course-toolbar-error'
        ];
        $data->footer = '';

        return $data;
    }
}