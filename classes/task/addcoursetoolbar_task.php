<?php

/**
 * Task scheduled by CRON:
 *  check if a course has been an instance of block course_toolbar and add it if not.
 *
 * @package     block_course_toolbar
 * @author      Nicolas Daugas
 * @copyright   2020 Académie de Versailles
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_course_toolbar\task;

use core\task\scheduled_task;
use stdClass;

class addcoursetoolbar_task extends scheduled_task {

    const COLOR_INFO    = "\033[0;36m";
    const COLOR_SUCCESS = "\033[0;32m";
    const COLOR_WARNING = "\033[0;33m";
    const COLOR_DANGER  = "\033[0;31m";
    const COLOR_DEFAULT = "\033[m";

    public function get_name()
    {
        return get_string('addcoursetoolbartask', 'block_course_toolbar');
    }

    public function execute()
    {
        global $DB;

        // Add block for older courses if not done
        $coursecontextwithoutblock = $DB->get_records_sql('
                SELECT id
                FROM {context}
                WHERE contextlevel = :coursecontext AND instanceid <> 1 AND id NOT IN (
                  SELECT cont.id FROM {context} AS cont 
                  LEFT JOIN {block_instances} AS bl 
                  ON cont.contextlevel = :coursecontext2 AND bl.parentcontextid = cont.id 
                  WHERE blockname = "course_toolbar")'
                ,
            [
                'coursecontext' => CONTEXT_COURSE,
                'coursecontext2' => CONTEXT_COURSE
            ]
        );

        foreach ($coursecontextwithoutblock as $context) {
            $blockinstance = new stdClass();
            $blockinstance->blockname = 'course_toolbar';
            $blockinstance->parentcontextid = $context->id;
            $blockinstance->showinsubcontexts = 0;
            $blockinstance->pagetypepattern = 'course-view-*';
            $blockinstance->subpagepattern = null;
            $blockinstance->defaultregion = 'side-pre';
            $blockinstance->defaultweight = 0;
            $blockinstance->configdata = '';
            $blockinstance->timecreated = time();
            $blockinstance->timemodified = $blockinstance->timecreated;
            $blockinstance->id = $DB->insert_record('block_instances', $blockinstance);
        }

        $this->ctrace(count($coursecontextwithoutblock) . " course toolbar added", self::COLOR_SUCCESS);
    }

    /**
     * Colored trace
     * @param $msg
     * @param string $color
     */
    private function ctrace($msg, $color = self::COLOR_DEFAULT)
    {
        mtrace($color . $msg . self::COLOR_DEFAULT);
    }
}
