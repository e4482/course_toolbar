<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings for the recentlyaccessedcourses block
 *
 * @package    block_recentlyaccessedcourses
 * @copyright  2019 Mihail Geshoski <mihail@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    // Display Course Categories on the recently accessed courses block items.
    $settings->add(new admin_setting_configcheckbox(
        'block_course_toolbar/allowmovingtotop',
        get_string('allowmovingtotop', 'block_course_toolbar'),
        get_string('allowmovingtotop_help', 'block_course_toolbar'),
        1));
    $settings->add(new admin_setting_configcheckbox(
        'block_course_toolbar/stickybar',
        get_string('stickybar', 'block_course_toolbar'),
        get_string('stickybar_help', 'block_course_toolbar'),
        1));
    $settings->add(new admin_setting_configtext(
        'block_course_toolbar/highlighted_modules',
        get_string('highlighted_modules', 'block_course_toolbar'),
        get_string('highlighted_modules_help', 'block_course_toolbar'),
        ''));
    $settings->add(new admin_setting_confightmleditor(
        'block_course_toolbar/default_title',
        get_string('default_title_label', 'block_course_toolbar'),
        get_string('default_title_help', 'block_course_toolbar'),
        null));
    $settings->add(new admin_setting_confightmleditor(
        'block_course_toolbar/default_description',
        get_string('default_description_label', 'block_course_toolbar'),
        get_string('default_description_help', 'block_course_toolbar'),
        null));
    $settings->add(new admin_setting_confightmleditor(
        'block_course_toolbar/default_launcher',
        get_string('default_launcher_label', 'block_course_toolbar'),
        get_string('default_launcher_help', 'block_course_toolbar'),
        null));
}
