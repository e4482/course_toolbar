<?php
/**
 * This file is part of course_toolbar Moodle block plugin.
 *
 * course_toolbar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * course_toolbar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Knowledgegate.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package course_toolbar
 * @author Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
 * @copyright 2020 Académie de Versailles
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Plugin mandatory
$string['title'] = 'Barre d\'édition';
$string['pluginname'] = 'Course toolbar';
$string['course_toolbar:addinstance'] = 'Ajouter une barre d\'outil pour l\'édition de parcours';

// Categories
$string['presentation'] = 'Présentation';
$string['navigation'] = 'Navigation';
$string['resourcemodules'] = 'Ressources';
$string['activitymodules'] = 'Activités';
$string['completion'] = 'Achèvements';
$string['availability'] = 'Restrictions';
$string['users'] = 'Participants';

// Content
$string['maketitlebutton'] = 'Ajouter un titre';
$string['makedescriptionbutton'] = 'Ajouter une description';
$string['makelauncherbutton'] = 'Ajouter un bouton de lancement';
$string['dummymodulenameforlauncher'] = 'Lancez-vous !';
$string['availabilityremoveall'] = "Retirer toutes les restrictions";
$string['availabilitystepbystep'] = "Rendre les activités disponibles l'une après l'autre";
$string['availabilitystepbystepexcludesection0'] = "Exclure la section 0";
$string['completionremove'] = "Retirer toutes les conditions d'achèvement";
$string['completionreset'] = "Réinitialiser aux valeurs par défaut";
$string['reloadwaitingmessage'] = 'Fait. Veuillez patienter.';
$string['modifiedmodulesnotification'] = 'modules modifiés';
$string['dummymodulenameforlauncher'] = 'Lancez-vous !';
$string['enrolledusers'] = 'Gérer les inscrits';
$string['gradeadministration'] = 'Accéder aux notes';
$string['ajaxerrormessage'] = "Une erreur s'est produite, essayer de recharger la page, s'il vous plaît.;";

// Settings
$string['allowmovingtotop'] = "Barre d'outils en haut du parcours";
$string['allowmovingtotop_help'] = "Force la position de la barre d'outils pour qu'elle soit en haut du parcours.";
$string['stickybar'] = "Position fixée";
$string['stickybar_help'] = "La barre d'outils reste fixée lorsqu'on déroule la page du parcours. 
Cela marche quelque soit la position de la barre d'outils, elle peut recouvrir d'autres blocs si elle est laissée sur le côté.";
$string['highlighted_modules'] = "Modules favoris";
$string['highlighted_modules_help'] = "Modules à mettre dans le panneau des modules favoris qui apparaît en premier.";
$string['default_title_label'] = 'Titre généré';
$string['default_title_help'] = "Contenu du titre généré par le bouton de la barre d'outils";
$string['default_description_label'] = 'Description générée';
$string['default_description_help'] = "Contenu de la description générée par le bouton de la barre d'outils";
$string['default_launcher_label'] = 'Bouton de lancement générée';
$string['default_launcher_help'] = "Contenu du bouton de lancement générée par le bouton de la barre d'outils. 
Il doit contenir la chaîne de caractère TARGETED_MODULE qui sera automatiquement remplacée par le nom du premier module.";

// Task
$string['addcoursetoolbartask'] = "Ajouter la barre d'édition à tous les parcours";