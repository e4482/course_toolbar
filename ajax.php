<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 27/03/2020
 * Time: 16:43
 */

use block_course_toolbar\local\actions\responses\ResponseError;
use block_course_toolbar\local\actions\responses\ResponseException;

define('AJAX_SCRIPT', true);
global $DB, $USER;

require_once(__DIR__ . '/../../config.php');

$courseid = required_param('courseid', PARAM_TEXT);
$category = required_param('category', PARAM_TEXT);
$action = required_param('action', PARAM_TEXT);

require_login($courseid);

$className = "block_course_toolbar\local\actions\\${category}AjaxAction";
$actionHandler = new $className($courseid);
if (method_exists($actionHandler, $action)) {
    try {
        $response = $actionHandler->$action();
    } catch (Exception $exception) {
        $response = new ResponseException($exception);
    }
} else {
    $response = new ResponseError();
    $response->add_error("Action $action doesn't match the category $category");
}
echo $response->render();
