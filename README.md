Ce bloc a pour but de simplifier l'édition de parcours en proposant une bar d'édition à la manière d'un éditeur de texte moderne.



This block aims to simplify the course edition by adding a toolbar similar to modern text editors.