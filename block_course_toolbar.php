<?php
/**
 * This file is part of course_toolbar Moodle block plugin.
 *
 * course_toolbar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * course_toolbar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Knowledgegate.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package course_toolbar
 * @author Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
 * @copyright 2020 Académie de Versailles
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use block_course_toolbar\local\CourseToolbar;
use block_course_toolbar\output\course_toolbar;

defined('MOODLE_INTERNAL') || die();

class block_course_toolbar extends block_base {

    /**
     * Initialisation: only preparing constants.
     * @throws coding_exception
     */
    public function init() {
        $this->title = get_string('title', 'block_course_toolbar');
    }

    /**
     * This block is displayed only at course level.
     */
    public function applicable_formats()
    {
        return array(
            'admin' => false,
            'site-index' => false,
            'course-view' => true,
            'mod' => false,
            'my' => false

        );
    }

    /**
     * Applying template to render.
     * @return stdClass|stdObject|string
     * @throws coding_exception
     */
    public function get_content()
    {
        global $PAGE;
        $PAGE->requires->js('/blocks/course_toolbar/js/courseToolbar.js');
        $course = $PAGE->course;


        // Be sure not to compute it twice (see documentation)
        if ($this->content !== null) {
            return $this->content;
        }

        // check to see if user can add menus and there are modules to add
        if (!has_capability('moodle/course:manageactivities', context_course::instance($course->id))
            || !$this->page->user_is_editing()) {
            return '';
        }

        // Render content
        $renderer = $this->page->get_renderer('block_course_toolbar');
        $coursetoolbar = new CourseToolbar($course);
        $this->content = new stdClass();
        $this->content->text = $renderer->render($coursetoolbar);
        $this->content->footer = "";

        return $this->content;
    }

    /**
     * Enabling configuration.
     * @return bool
     */
    public function has_config()
    {
        return true;
    }
}